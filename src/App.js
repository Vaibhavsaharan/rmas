import { Component } from 'react';
import  { lazy, Suspense } from 'react';  
import Info from './components/Info';
//import Allrecipescomp from './components/Allrecipescomp';
import Incorrectcomp from './components/Incorrectcomp';
import Untaggedcomp from './components/Untaggedcomp';
import Disabledcomp from './components/Disabledcomp';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import Tablestyle from './components/Table.module.css';
import { BounceLoader } from 'react-spinners';
const Allrecipescomp = lazy(() => import('./components/Allrecipescomp'))

class App extends Component {
  constructor(){
    super()
    this.state = {
        recipes : [],
        isloaded : false,
        sortfield: "",
        ascending: true,
        color:'white'
    }
  }

  componentDidMount(){
  var todoIdList = [1, 2, 3]
  Promise.all(
    todoIdList.map(id => {
      return new Promise((resolve) => {
        fetch("https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes/?page="+id)
          .then(response => {
            return new Promise(() => {
              response.json()
                .then(todo => {
                  todo.results.map(rec =>(
                    this.setState(prevState => ({
                      isloaded : true,
                      recipes: [...prevState.recipes, rec]
                    })))
                  )
                  resolve()
                })
            })
          })
      })
    })
  )
  }
  render(){
    return (
      <div className="App">
        <Info />
        <Tabs className={Tablestyle.wrapper}>
          <TabList className={Tablestyle.list}>
            <Tab>ALL RECIPE(S)</Tab>
            <Tab>INCORRECT</Tab>
            <Tab>UNTAGGED</Tab>
            <Tab>DISABLED</Tab>
          </TabList>
          <TabPanel className={Tablestyle.panel}>
            <Suspense fallback={<BounceLoader className={Tablestyle.loader}>
              color={'white'}
              loading = {false}
            </BounceLoader>}>
              <Allrecipescomp recipes = {this.state.recipes}/>
            </Suspense>
          </TabPanel>
          <TabPanel className={Tablestyle.panel}>
            <Incorrectcomp recipes = {this.state.recipes}/>
          </TabPanel>
          <TabPanel className={Tablestyle.panel}>
            <Untaggedcomp recipes = {this.state.recipes}/>
          </TabPanel>
          <TabPanel className={Tablestyle.panel}>
            <Disabledcomp recipes = {this.state.recipes}/>
          </TabPanel>
        </Tabs>
      </div>
    )
  }
}

export default App;
