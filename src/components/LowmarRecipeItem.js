import ProgressRing from './ProgressRing'
import Infostyle from './Info.module.css'

function LowmarRecipeItem(props){
    return (
        <div className={Infostyle.vcard}>
            <h5>{props.item.name}</h5>
            <ProgressRing radius={40 } stroke={4} color ={'red'} progress = {props.item.margin} newcolor={'rgb(199, 156, 156)'}/>
        </div>
    );
}

export default LowmarRecipeItem