function RecipeItem(props){
    console.log(props)
    var recipe = props.item

    return (
        <tr key={recipe.id}>
            <td>{recipe.name}</td>
            <td>{recipe.last_updated}</td>
            <td>{recipe.cogs}</td>
            <td>{recipe.cost_price}</td>
            <td>{recipe.sale_price}</td>
            <td>{recipe.gross_margins}</td>
            <td>{recipe.name}</td>
        </tr>
    );
}

export default RecipeItem
// {/* <th><button type="button" onClick={() => this.setState({sortfield:"name"})}>NAME</button></th>
//                     <th><button type="button" onClick={() => this.setState({sortfield:"last_updated"})}>LAST UPDATED</button></th>
//                     <th><button type="button" onClick={() => this.setState({sortfield:"cogs"})}>COGS%</button></th>
//                     <th><button type="button" onClick={() => this.setState({sortfield:"cost_price"})}>COST PRICE(INR)</button></th>
//                     <th><button type="button" onClick={() => this.setState({sortfield:"sale_price"})}>SALE PRICE</button></th>
//                     <th><button type="button" onClick={() => this.setState({sortfield:"gross_margin"})}>GROSS MARGIN</button></th>
//                     <th><button type="button" onClick={() => this.setState({sortfield:"name"})}>TAGS/ACTION</button></th> */}