import { Component } from 'react';
import TopflucRecipeItem from './TopflucRecipeItem';
import Infostyle from './Info.module.css'

class Topfluc extends Component{
    constructor(){
        super()
        this.state = {
            topflucrecipes : [],
        }
    }

    componentDidMount(){
    fetch("https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/fluctuation-group/?order=top")
        .then(res => res.json())
        .then(data => {
            this.setState({
                topflucrecipes : data.results,
            })
        }) 
    }
    render(){
        var topflucrecipes = this.state.topflucrecipes
        const topflucitems = topflucrecipes.map(toprecipe => <TopflucRecipeItem key={toprecipe.name} item = {toprecipe}/>)
        return (
            <div className ={Infostyle.scard}>
                <h3>Top Fluctuating Recipes</h3>
                <ul className={Infostyle.item}>{topflucitems}</ul>
            </div>
        )
    }
}

export default Topfluc