
function RecipeItem(props){
    console.log(props)
    var recipe = props.item

    return (
        <tr key={recipe.id}>
            <td>{recipe.name}</td>
            <td>{recipe.last_updated}</td>
            <td>{recipe.cogs}</td>
            <td>{recipe.cost_price}</td>
            <td>{recipe.sale_price}</td>
            <td>{recipe.gross_margins}</td>
            <td>{recipe.name}</td>
        </tr>
    );
}

export default RecipeItem