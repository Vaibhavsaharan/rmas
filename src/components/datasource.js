export const columns = [
    {
        Header: 'NAME',
        accessor: 'name'
    },
    {
        Header: 'LAST UPDATED',
        accessor: 'last_updated.date'
    },
    {
        Header: 'COGS%',
        accessor: 'cogs'
    },
    {
        Header: 'COST PRICE(INR)',
        accessor: 'cost_price'
    },
    {
        Header: 'SALE PRICE',
        accessor: 'sale_price'
    },
    {
        Header: 'GROSS MARGIN',
        accessor: 'gross_margin'
    },
    {
        Header: 'TAGS/ACTION',
        accessor: 'namemanufacturing_outlet'
    },
  ];
  
