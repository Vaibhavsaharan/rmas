import { Component } from 'react';
import LowmarRecipeItem from './LowmarRecipeItem';
import Infostyle from './Info.module.css'

class Lowmar extends Component{
    constructor(){
        super()
        this.state = {
            lowmarrecipes : [],
        }
    }

    componentDidMount(){
    fetch("https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/margin-group/?order=bottom")
        .then(res => res.json())
        .then(data => {
            this.setState({
                lowmarrecipes : data.results,
            })
        }) 
    }
    render(){
        var lowmarrecipes = this.state.lowmarrecipes
        const lowmaritems = lowmarrecipes.map(lowrecipe => <LowmarRecipeItem key={lowrecipe.name} item = {lowrecipe}/>)
        return (
            <div className ={Infostyle.scard}>
                <h3>Low Margin Recipes</h3>
                <ul className={Infostyle.item}>{lowmaritems}</ul>
            </div>
        )
    }
}

export default Lowmar