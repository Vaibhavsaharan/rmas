import { Component } from "react"
import Highmar from './Highmar';
import Lowmar from './Lowmar';
import Topfluc from './Topfluc';
import Infostyle from './Info.module.css'

class Info extends Component{
    render(){
        return (
            <div className ={Infostyle.card}>
                <Highmar />
                <Lowmar />
                <Topfluc />
            </div>
        )
    }
}

export default Info