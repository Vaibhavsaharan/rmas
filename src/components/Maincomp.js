import React from 'react'
import { useTable, useSortBy } from 'react-table'
import styled from 'styled-components'
import { columns } from './datasource';

const Styles = styled.div`

  table {
    border-spacing: 0;
    border-radius: 5px;
    font-size: 14px;
    font-weight: normal;
    border: none;
    width:100%;
    border-collapse: collapse;
    white-space: nowrap;
    background-color: white;

    thead tr{
        color: #ffffff;
        background: #4FC3A1;
    }
    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
      :nth-child(even){
        background: #F8F8F8;
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-right: 1px solid #f8f8f8;

      :last-child {
        border-right: 0;
      }
      
    }
  }
`



function Table({ columns, data }) {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable(
    {
      columns,
      data,
    },
    useSortBy
  )

  // We don't want to render all 2000 rows for this example, so cap
  // it at 20 for this use case
  const firstPageRows = rows.slice(0, 20)

  return (
    <>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map(headerGroup => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(column => (
                // Add the sorting props to control sorting. For this example
                // we can add them into the header props
                <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                  {column.render('Header')}
                  {/* Add a sort direction indicator */}
                  <span>
                    {column.isSorted
                      ? column.isSortedDesc
                        ? ' 🔽'
                        : ' 🔼'
                      : ''}
                  </span>
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {firstPageRows.map(
            (row, i) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map(cell => {
                    return (
                      <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                    )
                  })}
                </tr>
              )}
          )}
        </tbody>
      </table>
    </>
  )
}

function Allrecipescomp(props) {

   const data = props.recipes  
  return (
    <Styles>
      <Table columns={columns} data={data} />
    </Styles>
  )
}
export default Allrecipescomp