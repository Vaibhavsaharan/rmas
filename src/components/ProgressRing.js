import { Component } from "react";
import Infostyle from './Info.module.css'

class ProgressRing extends Component{
    constructor(props) {
        super(props);

        const { radius, stroke } = this.props;

        this.normalizedRadius = radius - stroke * 2;
        this.circumference = this.normalizedRadius * 2 * Math.PI;
    }

    render(){
        const { radius, stroke, color, newcolor, progress} = this.props;
        const strokeDashoffset = this.circumference - progress / 100 * this.circumference;
        const understrokeDashoffset = 0
        return(
            <div>
                <svg transform='rotate(270)' height={radius * 2} width={radius * 2}>
                <circle
                        stroke={newcolor}
                        fill="transparent"
                        strokeWidth={ stroke }
                        strokeDasharray={ this.circumference + ' ' + this.circumference }
                        style={ { understrokeDashoffset } }
                        r={ this.normalizedRadius }
                        cx={ radius }
                        cy={ radius }
                    />
                    <circle
                        stroke={color}
                        fill="transparent"
                        strokeWidth={ stroke }
                        strokeDasharray={ this.circumference + ' ' + this.circumference }
                        style={ { strokeDashoffset } }
                        r={ this.normalizedRadius }
                        cx={ radius }
                        cy={ radius }
                    />
                </svg>
                <font style={{color : newcolor === "#12931243" ? "rgb(68 209 68)" : "rgb(255 163 163)" }} className={Infostyle.txt} >{progress}%</font>
            </div>
            
        )
    }
}

export default ProgressRing