import { Component } from 'react';
import HighmarRecipeItem from './HighmarRecipeItem';
import Infostyle from './Info.module.css'

class Highmar extends Component{
    constructor(){
        super()
        this.state = {
            highmarrecipes : [],
        }
    }

    componentDidMount(){
    fetch("https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/margin-group/?order=top")
        .then(res => res.json())
        .then(data => {
            this.setState({
                highmarrecipes : data.results,
            })
        }) 
    }
    render(){
        var highmarrecipes = this.state.highmarrecipes
        const highmaritems = highmarrecipes.map(highrecipe => <HighmarRecipeItem key={highrecipe.name} item = {highrecipe}/>)
        return (
            <div className ={Infostyle.scard}>
                <h3>High Margin Recipes</h3>
                <ul className={Infostyle.item}>{highmaritems}</ul>
            </div>
            
        )
    }
}

export default Highmar