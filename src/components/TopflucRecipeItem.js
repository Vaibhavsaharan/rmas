import ProgressRing from './ProgressRing'
import Infostyle from './Info.module.css'

function TopflucRecipeItem(props){
    return (
        <div className={Infostyle.vcard}>
            <h5>{props.item.name}</h5>
            <ProgressRing radius={40 } stroke={4} color ={'green'} newcolor={'#12931243'} progress = {props.item.fluctuation}/>
        </div>
    );
}

export default TopflucRecipeItem